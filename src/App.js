import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <img src='https://www.designevo.com/res/templates/thumb_small/line-structure-circle-bottle-test.webp'></img><br></br>
      <b>Joni Kontu</b><br></br><br></br>

      <b>Opiskelija</b><br></br>
      <span>Tietotekniikka</span><br></br>
      <span>VAMK</span><br></br>

      <br></br>
      <b>Student</b><br></br>
      <span>Information Technology</span><br></br>
      <span>VAMK</span><br></br>

      <br></br>
      <span>e2101369@edu.vamk.fi</span><br></br>
      <span>Esimerkkitie 1, 65200 Vaasa, Finland</span><br></br>
    </div>
  );
}

export default App;
